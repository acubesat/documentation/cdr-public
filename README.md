# AcubeSAT Critical Design Review

This repository contains the public PDF files of AcubeSAT's Critical Design Review.

The CDR contains the detailed design of the AcubeSAT nanosatellite, including
descriptions, analyses, justifications, and procedures.

## Directories
- **DDJF**: Design Definition and Justification Files, including:
	- DDJF_AOCS.pdf (Attitude & Orbit Control System)
	- DDJF_EMC.pdf (Electromagnetic Compatibility)
	- DDJF_GS.pdf (Ground Segment)
	- DDJF_LINK_ARPT.pdf (Link and Data Budget Analysis Report)
	- DDJF_MCH.pdf (Mechanisms)
	- DDJF_OBDH.pdf (On Board Data Handling)
	- DDJF_OBSW.pdf (On Board Software)
	- DDJF_PL.pdf (Payload)
	- DDJF_SYS.pdf (System)
	- DDJF_TTC.pdf (Telemetry, Tracking & Command)
- **FMEA**: Failure Mode & Effects Analysis, including:
   - FMEA.pdf (Failure Mode and Effects Analysis)
   - For the FMEA worksheet, refer to [`acubesat/systems-engineering/fmea`](https://gitlab.com/acubesat/systems-engineering/fmea)
- **MAIVP**: AIV Operations & Plan, including:
    - MAIVP.pdf (Manufacturing, Assembly, Integration and Verification Plan)
- **MDO**: Mission Description & Operations Plan, including:
	- MDO.pdf (Mission Description & Operations)
- **Mission Analysis Reports**: Reports used as appendices of the MDO file, including:
	- BMA_ARPT.pdf (Baseline Mission Appendix Analysis Report)
	- RAA_ARPT.pdf (Radiation Appendix Analysis Report)
	- SAA_ARPT.pdf (Sensitivity Analysis Appendix Report)
- **SDMR**: Space Debris Mitigation Report and supporting files, including:
    - SDMR.pdf (Space Debris Mitigation Report)
	- MDAR_ARPT.pdf (Mission Disposal Analysis Report)
	- OCRA_ARPT.pdf (On-orbit Collision Risk Assessment Analysis Report)
	- RECRA_ARPT.pdf (Re-Entry Casualty Risk Analysis Report)
- **SWRS**: Report regarding our Software Repository System, including:
	- SWRS.pdf (Software Repository System)

## Disclaimers
The AcubeSAT project is carried out with the support of the Education Office of the [European Space Agency](https://www.esa.int/), under the educational [Fly Your Satellite!](https://www.esa.int/Education/CubeSats_-_Fly_Your_Satellite/) programme.

All documents contain the analysis approaches, along with their justification and description, as well as the required documentation, all in compliance with the [European Space Agency](https://www.esa.int/) Policy.

All documents will remain open until the later stages of the project and therefore will be updated on a regular basis during the different [Fly Your Satellite! 3](https://www.esa.int/Education/CubeSats_-_Fly_Your_Satellite) phases.

This repository has been authored by university students participating in the AcubeSAT project. The views expressed herein by the authors can in no way be taken to reflect the official opinion, or endorsement, of the [European Space Agency](https://www.esa.int/).

The documentation in this repository is provided "as is", without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose and noninfringement. In no event shall the authors or copyright holders be liable for any claim, damages or other liability, whether in an action of contract, tort or otherwise, arising from, out of or in connection with the documentation or the use or other dealings in the documentation.
